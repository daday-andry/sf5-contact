<?php

namespace App\Controller;

use App\Entity\Contact\Message;
use App\Service\Utils\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="contact")
     */
    public function contactSendHandler(Request $request, EntityManagerInterface $em)
    {
        $data = $request->request;

        $message = new Message();
        $message->setFromName($request->get("txtName"));
        $message->setFromPhone($request->get("txtPhone"));
        $message->setFromEmail($request->get("txtEmail"));
        $message->setObject($request->get("txtObject"));
        $message->setContent($request->get("txtMsg"));
        $message->setdate(new \DateTime());

        $em->persist($message);
        $em->flush();
        
        return new JsonResponse(['success'=>true]);
    }

     /**
     * @Route(path="/contact/success", name="contact_success")
     */
    public function contactSuccess(){
        return $this->render("contact/success.html.twig");
    }
}
