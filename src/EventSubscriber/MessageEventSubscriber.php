<?php

namespace App\EventSubscriber;

use App\Entity\Contact\Message;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use App\Service\Utils\Mailer;
use Doctrine\ORM\EntityManagerInterface;


class MessageEventSubscriber implements EventSubscriber
{
   /**@var Mailer service*/
   private $mailer;

   private $entityManager;

  public function __construct(Mailer $mailer)
  {
    $this->mailer = $mailer;
  }
  public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $message = $args->getObject();

        
        if ($message instanceof Message) {
            $this->mailer->sendContactMessage($message);
          }
    }
}