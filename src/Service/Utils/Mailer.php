<?php


namespace App\Service\Utils;

use App\Entity\Contact\Message;
use App\Entity\Practitioner\Invitation;
use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use App\Entity\RdvMedical;

class Mailer
{
    /**
     * @var MailerInterface
     */
    private $sender;
    /**
     * @var string
     */
    private $fromEmail;
    /**
     * @var RouterInterface
     */
    private $router;
    public function __construct(MailerInterface $mailer, RouterInterface $router)
    {
        $this->fromEmail = $_ENV['FROM_EMAIL'];
        $this->sender = $mailer;
        $this->router = $router;
    }
    public function sendContactMessage(Message $message)
    {
        $email = (new TemplatedEmail())
            ->from($this->fromEmail)
            ->to($this->fromEmail)
            ->subject($message->getObject())
            ->htmlTemplate('emails/contact/message.html.twig')
            ->context([
                'message' => $message
            ]);
        $this->sender->send($email);
    }
    public function sendPingMail($to)
    {
        $email = (new TemplatedEmail())
            ->from($this->fromEmail)
            ->to($to)
            ->subject('Ping teste mail !')
            ->htmlTemplate('emails/ping.html.twig')
            ->context([
                'link' => $url
            ]);
        $this->sender->send($email);
    }
}